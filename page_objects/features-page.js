const scheduleDemoBtn = element(by.xpath('//*[@id="container"]/div[1]/div[2]/div/div/div[3]/figure/a'));

const BasePage = require('./base-page');

class FeaturesPage extends BasePage {
  constructor() {
    super();
  }

  clickScheduleDemo() {
    browser.wait(this.EC.visibilityOf(scheduleDemoBtn, 5000));
    scheduleDemoBtn.click();
  }
}

module.exports = FeaturesPage;