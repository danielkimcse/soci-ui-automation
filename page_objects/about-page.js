const viewAvailBtn = element(by.xpath('//*[@id="container"]/div[10]/div/div/div/div[2]/div[2]/div/div/div/figure/a'));

const BasePage = require('./base-page');

class AboutPage extends BasePage {
  constructor() {
    super();
  }
  clickViewAvail() {
    browser.wait(this.EC.visibilityOf(viewAvailBtn, 5000));
    viewAvailBtn.click();
  }
}

module.exports = AboutPage;