const about = element(by.xpath("(//a[contains(text(),'About')])[2]"));;
const features = element(by.xpath("(//a[contains(text(),'Features')])[2]"));;
const login = element(by.xpath("(//a[contains(text(),'Login')])[2]"));
const contact = element(by.xpath('//*[@id="menu-item-8761"]/a'));

const BasePage = require('./base-page');

class MainPage extends BasePage {
  constructor() {
    super();
  }

  clickAbout() {
    browser.wait(this.EC.visibilityOf(about, 5000));
    about.click();
  }
  
  clickContact() {
    browser.wait(this.EC.visibilityOf(contact, 5000));
    contact.click();
  }

  clickLogin() {
    browser.wait(this.EC.visibilityOf(login, 5000));
    login.click();
    browser.getAllWindowHandles().then((handles) => {
      browser.switchTo().window(handles[1]);
    });
  }

  clickFeatures() {
    browser.wait(this.EC.visibilityOf(features, 5000));
    features.click();
  }
}

module.exports = MainPage