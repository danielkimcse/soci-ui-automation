const protractor = require('protractor');

class BasePage {
  constructor() {
    if (this.constructor === BasePage) {
      throw new TypeError('Abstract class "BasePage" cannot be instantiated directly');
    }

    this.EC = protractor.ExpectedConditions;
  }

  goTo(url) {
    return browser.get(url);
  }

  getUrl() {
    return browser.getCurrentUrl();
  }

  closeCurrentTab() {
    browser.getAllWindowHandles().then((handles) => {
      browser.driver.switchTo().window(handles[1]);
      browser.driver.close();
      browser.driver.switchTo().window(handles[0]);
    });
  }
}

module.exports = BasePage;