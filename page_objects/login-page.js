const emailField = element(by.xpath('//*[@id="content"]/div/div/form/input[1]'));
const pwdField = element(by.xpath('//*[@id="content"]/div/div/form/input[2]'));

const loginBtn = element(by.css('#content > div > div > form > button'));
const errorMsgTitle = element(by.xpath('/html/body/div[9]/div/div[1]/h3'));
const errorMsg = element(by.xpath('/html/body/div[9]/div/div[2]'));

const BasePage = require('./base-page');

class LoginPage extends BasePage {
  constructor() {
    super();
  }

  typeEmail(text) {
    browser.wait(this.EC.visibilityOf(emailField, 5000));
    emailField.sendKeys(text)
  }

  typePwd(text) {
    browser.wait(this.EC.visibilityOf(pwdField, 5000));
    pwdField.sendKeys(text)
  }

  clickLogin() {
    browser.wait(this.EC.visibilityOf(loginBtn, 5000));
    loginBtn.click();
  }

  getErrorMsgTitle() {
    browser.wait(this.EC.visibilityOf(errorMsgTitle, 5000));
    return errorMsgTitle.getText();
  }
  
  getErrorMsg() {
    browser.wait(this.EC.visibilityOf(errorMsgTitle, 5000));
    return errorMsg.getText();
  }
}

module.exports = LoginPage;