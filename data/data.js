"use strict";
exports.data = {
	"aboutUsTxt": "About Us",
	"aboutUsType": "about us",
	"contactLink": "contact/",
	"careersLink": "careers/",
	"sdetTxt": "Software Development Engineer in Test",
	"searchLink": "?s=",
	"errMsgTitle": "Unable to Login",
	"loginInvalidEmail": "invalid@invalid.com",
	"loginValidEmail": "admin@meetsoci.com",
	"loginInvalidPwd": "invalid",
	"errInvalidEmail": "Invalid Email",
	"errInvalidPwd": "Invalid Password"
};
