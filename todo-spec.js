//page objects
const MainPage = require('./page_objects/main-page');
const ContactPage = require('./page_objects/contact-page');
const FeaturesPage = require('./page_objects/features-page');
const LoginPage = require('./page_objects/login-page');
const AboutPage = require('./page_objects/about-page');

//protractor properties
var EC = protractor.ExpectedConditions;

//initializing page object classes
var mainPage = new MainPage();
var contactPage = new ContactPage();
var featuresPage = new FeaturesPage();
var loginPage = new LoginPage();
var aboutPage = new AboutPage();

var baseUrl = require('./conf.js').config.baseUrl;
var data = require('./data/data.js').data;

//start of test
describe('simple soci website test', () => {
   beforeEach(() => {
    browser.waitForAngularEnabled(false); //to test non-angular webs
    browser.get(baseUrl); //open main page
   });

  it('should open contact page', () => {
    mainPage.clickContact();
    expect(contactPage.getUrl()).toEqual(baseUrl + data.contactLink); //checking by url
  });
  it('should display unable to login message when attempting to log in with blank credentials', () => {
    mainPage.clickLogin();
    loginPage.clickLogin();
    expect(loginPage.getErrorMsgTitle()).toEqual(data.errMsgTitle); //"Unable to Login"
    loginPage.closeCurrentTab();
  });
  it('should display invalid email when attempting to log in with an invalid email', () => {
    mainPage.clickLogin();
    loginPage.typeEmail(data.loginInvalidEmail); //"invalid@invalid.com"
    loginPage.clickLogin();
    expect(loginPage.getErrorMsg()).toEqual(data.errInvalidEmail); //"Invalid Email"
    loginPage.closeCurrentTab();
  });
  it('should display invalid password when attempting to log in with an invalid password', () => {
    mainPage.clickLogin();
    loginPage.typeEmail(data.loginValidEmail); //"admin@meetsoci.com"
    loginPage.typePwd(data.loginInvalidPwd); //"invalid"
    loginPage.clickLogin();
    expect(loginPage.getErrorMsg()).toEqual(data.errInvalidPwd); //"Invalid Password"
    loginPage.closeCurrentTab();
  });

  it('should direct to contact page when "Schedule a demo today" button is clicked in features page', () => {
    mainPage.clickFeatures();
    featuresPage.clickScheduleDemo();
    expect(contactPage.getUrl()).toEqual(baseUrl + data.contactLink); //Bug
  });

  it('should direct to careers page when "View available careers" button is clicked in about page', () => {
    mainPage.clickAbout();
    aboutPage.clickViewAvail();
    expect(contactPage.getUrl()).toEqual(baseUrl + data.careersLink);
  });
});