#!/bin/bash
echo "1. Updating Selenium..."
if ! webdriver-manager update 1> /dev/null
then
    error_exit "Could not update webdriver-manager! Exiting..."
fi

echo "2. Starting Selenium..."
if ! webdriver-manager start 1> /dev/null &
then
    error_exit "Could not start webdriver-manager! Exiting..."
fi

